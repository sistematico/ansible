# Ansible

## Pre-install

```bash
ssh root@[IP] -p 22
passwd
dnf upgrade --refresh --allowerasing # Rocky Linux
systemctl enable NetworkManager # Rocky Linux
ssh-copy-id -p 22 root@[IP]
```

## Test

```bash
ansible webservers -m ping
```

## Usage

```bash
ansible-playbook playbook.yml -i inventory
```